# Teste 5

Responda as seguintes perguntas:

### Para você, o que é o código ideal?
- Existem alguns pontos que são extremamente importantes que eu avalio, são eles:
  1. O código deve existir por **um propósito**, deve atender à **todas** as necessidades do cliente.
  2. O código precisa funcionar! De nada adianta um código maravilhoso, se ele não faz nada.
  3. Até aí tudo ok. Depois vem a parte que os desenvolvedores consideram, **manutenabilidade**, é a capacidade de que este código seja mantido **por qualquer pessoa do time**. Que esteja no padrão do time, um exemplo básico é o hash do ruby: Alguns desenvolvedores usam a forma < 1.9.x que é `{ :key => 'value' }` e aí na mesma linha por exemplo, tem um código com a versão >= 2.0.x: `{ key: 'value' }`. Isso é extremamente importante pois, imagina a situação e o tempo de adaptação de uma pessoa para se integrar no time?
  4. **Inglês**, pra que escrever o código em português ou em mandarin, se o core da linguagem é escrita toda em Inglês? Ainda mais quando se utiliza frameworks como rails, não tem vantagem nenhuma trocar o inglês pelo português dentro do código. Pelo contrário além de atrapalhar na leitura, prejudica a internacionalização da sua equipe, você sabe se a sua empresa não pensa em pegar aquele dev super experiente dos EUA? Então!!
  5. Depois de bem seguidas estas 4 regras, documentação vem em segundo plano. Pois o código **bem escrito** e conciso **fica claro o qual é o seu propósito**. Existe também algumas *patterns* do xp que dizem, "não diga pra mim o que tenho que fazer." ou algo do gênero, mas que na prática significa: "Pow cara, pra que comentar uma linha `a = 2` com algo escrito: atribui `2` à variável `a`, é isso que eu faço, não precisa me ensinar!!".
  
### Descreva o passo a passo de um workflow git
  Elaborei um workflow para a empresa em que trabalho atualmente, segue o [link](https://github.com/internetsistemas/guides/tree/master/best-practices/git-workflow).
  Para mim basicamente, um bom workflow é aquele que te proporciona duas coisas:
  - 1° Code Review: Assim você pode se certificar se um código está legal ou não para entrar na branch `master` ou não. E fazer as devidas correções nele.
  - 2° Integração Contínua: Impossível!! trabalhar hoje sem um desses, TravisCI, CircleCI, Semaphore... enfim! É uma beleza, roda as specs, deixa isto claro, se falhou ou não, já automatiza os próximos passos, e diminui a possibilidade de erros na `master`, ou pior em produção!
  
  Sobre usar a forma de forks ou branchs, não tenho uma opinião definida, mas acredito que só o fato de ter que possuir dois remotes no seu repositório local, o `upstream` e o `origin`. Já me fazem querer adotar `branches`!!
  
### Qual o seu sistema operacional favorito? Justifique.
  Qualquer distro Linux hoje :). Mas pra mim a mais fantástica é o [antergos](http://antergos.org) (arch based), apesar de não usá-la o tempo que a utilizei achei simplesmente incrível, principalmente por implementar um visual bacana GNOME + Numix, perfeito! Sem contar que **qualquer** pacote baixado é sempre a última versão, a mais atualizada possível. Infelizmente meu computador falhava ao iniciar pois o recurso de vídeo não era utilizado às vezes e me deixava na mão!
  
  Windows pra mim é para usuário leigo (sem desmerecer por favor!), que não precisa de nada avançado e quer facilidade nas coisas e que não se importe de ter sua memória RAM no full, a menos que ele seja dev de tecnologias Microsoft, aí são outros 500.
  
  Infelizmente nunca tive um Mac (ainda).
  
  O Docker está na minha lista para ser aprendido, já vi várias vantagens no seu uso e já o testei também. Pois ele "emula" só o necessário do OS que você precisa tornando-o rápido de fazer o setup em uma máquina "nova" digamos e rápido no uso porque você só deixa lá as coisas necessárias.
  Pra resumir... Linux, Linux, Linux <3!
  
### Qual o seu framework favorito? Justifique.
  Já trabalhei com C# os frameworks: WebForms e ASP.NET MVC (4, 5). E brinquei com os frameworks a seguir. Com Python: Django, Bottle. Com Java: JSF, Play. Com Groovy o Grails. Em Ruby: Rails, Pakyow, Lotus, Sinatra. E não vi nada igual em quesito produtividade para bater o Rails, indiscutivelmente o melhor.
  
  Mas estou dedicando hoje em dia meu tempo extra a dominar o [Pakyow](http://pakyow.org), que é um framework muito bacana, acho que ele pode ser o divisor de águas para os Front-Ends, pois ele não mescla nenhum código ERB dentro de suas views, e isso é muito legal (só usa os atributos `data-` do HTML5), trás uma legibilidade monstra para o código tanto que o Front end faz para o Back End e vice e versa.
  
### Qual a sua linguagem de programação favorita? Justifique.
  Foi amor a primeira vista **Ruby** com toda a certeza.
  Primeiro de tudo pela sua missão imposta pelo Matz, que era tornar os programadores mais felizes, e com certeza ele conseguiu!
  Facilidade na compreensão do código, é simples de entender, parece texto plano, o uso dos **!** e **?**, foi algo genial!
  
  ```rb
    print 'I <3 Ruby!' if true_love?
    # Imprima I <3 Ruby! se o amor for verdadeiro!
  ```
  
  ```java
    if (trueLove == true) System.out.print("I <3 Java!");
    // Se o amor for... ah sem condições! hahahahahhaha
  ```
  Mas sem brincadeiras qual é a mais fácil de ser entendida em uma simples olhada?
  Também gosto muito da comunidade, sempre comparo com PHP, que é dividida, uns são Magento, outros Zend, Cake, Wordpress, talvez pela popularização tenha acontecido isto, o Ruby pode sofrer isso sim, mas até hoje a comunidade se parece bastante sólida.
  E, claro! o Rails tem sua enorme fatia... Pois qual linguagem tem um framework tão bacana quanto o Rails?!! Isto acaba contando muito!

### Coloque aqui o link para seu github para que possamos avaliar qualquer projeto anterior seu ou contribuição com projetos de código aberto.
  Perfil: https://github.com/brunoocasali
  Projetos feitos para a faculdade (Android, Rails, Java...) https://github.com/Unisep
  
Boa sorte!
