# Teste 1

## Primeira etapa

Construa uma aplicação em Rails CRUD (CREATE, READ, UPDATE, DELETE),  orientado a objetos.
Este CRUD deverá gerenciar Páginas Web, que são composta pelos seguintes campos:
Os campos a seguir são obrigatórios, sinta-se a vontade para criar outros campos se achar necessário.

+ id
+ title
+ slug
+ description
+ body
+ author
+ insert_date
+ update_date

= SemanticUI como framework
= Slim
= SimpleForm
= RSpec + FactoryGirl + Shoulda + DatabaseCleaner

## Segunda Etapa

Implementa os testes unitários para esta aplicação OK

## Terceira Etapa

Internacionalize a aplicação utilizando os recursos do Rails OK

## Quarta Etapa

Implemente cache na aplicação utilizando os recursos do Rails OK

## OBSERVAÇÕES IMPORTANTES

+ Não utilize o scaffold no teste
+ As rotas devem utilizar o padrão restfull

