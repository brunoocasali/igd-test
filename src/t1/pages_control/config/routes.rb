Rails.application.routes.draw do
  resources :pages
  resources :authors

  root to: 'pages#index'
end
