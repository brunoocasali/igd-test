class ApplicationController < ActionController::Base
  protect_from_forgery with: :exception

  responders :flash, :http_cache

  respond_to :html, :json
end
