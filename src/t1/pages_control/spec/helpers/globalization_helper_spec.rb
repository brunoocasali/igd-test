require 'rails_helper'

describe GlobalizationHelper, type: :helper do
  describe '.model_name' do
    it { expect(tm Author).to eq('Autor') }
    it { expect(tm Author, count: 2).to eq('Autores') }
  end

  describe '.model_name_pluralized' do
    it { expect(tmp Author).to eq('Autores') }
  end

  describe '.model_attribute' do
    it { expect(ta Author, :email).to eq('Email') }
  end
end
