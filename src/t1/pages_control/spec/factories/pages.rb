FactoryGirl.define do
  factory :page do
    title { Forgery(:lorem_ipsum).words(5) }
    slug { title.underscore.gsub!(' ', '-') }
    description { Forgery(:lorem_ipsum).text }
    body { Forgery(:lorem_ipsum).text }

    author
  end

  factory :invalid_page, parent: :page do
    title nil
    slug nil
    description nil
    body nil

    author nil
  end
end
