require 'rails_helper'

describe NoticeManagerHelper, type: :helper do
  describe '.notices' do
    it 'needs to show the right message' do
      flash[:alert] = 'A warning message'

      html_message = notices 500

      expect(html_message).to eq("<div class=\"ui compact closable message warning\"><span style=\"padding-right: 13px;\">A warning message</span><i class=\"close icon\" style=\"m\"></i></div>")
    end

    it 'needs to show the right message' do
      flash[:notice] = 'A warning message'

      html_message = notices 500

      expect(html_message).to eq("<div class=\"ui compact closable message success\"><span style=\"padding-right: 13px;\">A warning message</span><i class=\"close icon\" style=\"m\"></i></div>")
    end
  end
end
