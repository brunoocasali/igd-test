FactoryGirl.define do
  factory :author do
    name { Forgery(:name).full_name }
    email { Forgery(:internet).email_address }
  end

  factory :invalid_author, parent: :author do
    name nil
    email nil
  end
end
