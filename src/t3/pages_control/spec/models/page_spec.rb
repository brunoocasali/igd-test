require 'rails_helper'

RSpec.describe Page, type: :model do
  context 'database associations' do
    it { is_expected.to belong_to(:author) }
  end

  context 'model validations' do
    it { is_expected.to validate_presence_of(:author) }

    it { is_expected.to validate_presence_of(:description) }
    it { is_expected.to validate_presence_of(:body) }
    it { is_expected.to validate_presence_of(:slug) }
    it { is_expected.to validate_presence_of(:title) }
  end

  context 'model attributes' do
    it { is_expected.to have_db_column(:author_id).of_type(:integer) }
    it { is_expected.to have_db_column(:body).of_type(:text) }
    it { is_expected.to have_db_column(:description).of_type(:text) }
    it { is_expected.to have_db_column(:title).of_type(:string) }
    it { is_expected.to have_db_column(:created_at).of_type(:datetime) }
    it { is_expected.to have_db_column(:updated_at).of_type(:datetime) }
  end

  context 'database indexes' do
    it { is_expected.to have_db_index(:author_id) }
  end
end
