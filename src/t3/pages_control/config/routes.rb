Rails.application.routes.draw do
  ActiveAdmin.routes(self)
  resources :pages
  resources :authors

  root to: 'pages#index'
end
