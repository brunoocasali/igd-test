class Author < ActiveRecord::Base
  has_many :pages

  validates :name, presence: true
  validates :email, presence: true, uniqueness: true,
            format: /\A[^@]+@([^@\.]+\.)+[^@\.]+\z/

  def self.best_authors
    Author.joins(:pages).group(:id).select('authors.name, authors.email, authors.id, COUNT(*) as cc').order('cc ASC')
  end
end
