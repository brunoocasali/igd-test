ActiveAdmin.register_page 'Dashboard' do
  menu priority: 1, label: proc{ I18n.t("active_admin.dashboard") }

  content title: proc{ I18n.t("active_admin.dashboard") } do
    div class: "blank_slate_container", id: "dashboard_default_message" do
      span class: "blank_slate" do
        span I18n.t("active_admin.dashboard_welcome.welcome")
        small I18n.t("active_admin.dashboard_welcome.call_to_action")
      end
    end

    columns do
      column do
        panel t('active_admin.recent_pages') do
          ul do
            Page.order(:id).take(5).map do |page|
              li link_to page.title, [:admin, page]
            end
          end
        end
      end

      column do
        panel t('active_admin.recent_pages') do
          ul do
            Author.best_authors.take(5).map do |author|
              li link_to "#{author.name} / #{author.email}", [:admin, author]
            end
          end
        end
      end
    end
  end
end
