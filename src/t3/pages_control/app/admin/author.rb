ActiveAdmin.register Author do
  config.sort_order = 'name_asc'
  permit_params :name, :email

  index do
    column :name
    column :email

    column :created_at do |author|
      l author.created_at, format: :short
    end

    column tmp(Page), :pages do |author|
      author.pages.count
    end

    actions
  end

end
