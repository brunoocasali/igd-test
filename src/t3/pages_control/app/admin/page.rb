ActiveAdmin.register Page do

  index do
    column :title
    column :description
    column :author
    column :slug

    column :updated_at do |page|
      l page.updated_at, format: :short
    end

    column :created_at do |page|
      l page.created_at, format: :short
    end

    actions
  end

  permit_params :body, :description, :title, :author_id, :slug
end
