module NoticeManagerHelper
  def notices(timeout)
    flash.reject { |_, m| m.eql? true }.collect do |key, msg|
      content_tag :div, class: "ui compact closable message #{alert_class_finder(key)}" do
        concat content_tag(:span, msg, style: 'padding-right: 13px;')
        concat content_tag(:i, '', class: 'close icon', style: 'm')
      end
    end.join.html_safe
  end

  private

  def alert_class_finder(key)
    case key
    when 'notice'
      'success'
    when 'alert'
      'warning'
    end
  end
end
