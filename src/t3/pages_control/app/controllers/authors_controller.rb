class AuthorsController < ApplicationController
  before_action :set_author, only: [:show, :edit, :update, :destroy]

  def index
    @authors = Author.page

    respond_with(@authors)
  end

  def show
    respond_with(@author)
  end

  def new
    @author = Author.new

    respond_with(@author)
  end

  def edit; end

  def create
    @author = Author.new(author_params)
    @author.save

    respond_with(@author, location: authors_path)
  end

  def update
    @author.update(author_params)

    respond_with(@author, location: authors_path)
  end

  def destroy
    @author.destroy

    respond_with(@author, location: authors_path)
  end

  private

  def set_author
    @author = Author.find(params[:id])
  end

  def author_params
    params.require(:author).permit(:name, :email)
  end
end
