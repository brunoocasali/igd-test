require 'rails_helper'

RSpec.describe PagesController, type: :controller do
  let(:author) { create(:author) }
  let(:valid_attributes) { attributes_for(:page, author_id: author.id) }
  let(:invalid_attributes) { attributes_for(:invalid_page) }

  describe 'GET #index' do
    it 'assigns all pages as @pages' do
      page = create :page, valid_attributes

      get :index

      expect(assigns(:pages)).to match_array([page])
    end
  end

  describe 'GET #show' do
    it 'assigns the requested page as @page' do
      page = create :page, valid_attributes

      get :show, { id: page.to_param }

      expect(assigns(:page)).to eq(page)
    end
  end

  describe 'GET #new' do
    it 'assigns a new page as @page' do
      get :new

      expect(assigns(:page)).to be_a_new(Page)
    end
  end

  describe 'GET #edit' do
    it 'assigns the requested page as @page' do
      page = create :page, valid_attributes

      get :edit, { id: page.to_param }

      expect(assigns(:page)).to eq(page)
    end
  end

  describe 'POST #create' do
    context 'with valid params' do
      it 'creates a new Page' do
        expect {
          post :create, { page: valid_attributes }
        }.to change(Page, :count).by(1)
      end

      it 'assigns a newly created page as @page' do
        post :create, { page: valid_attributes }

        expect(assigns(:page)).to be_a(Page)
        expect(assigns(:page)).to be_persisted
      end

      it 'redirects to the created page' do
        post :create, { page: valid_attributes }

        expect(response).to redirect_to(Page.last)
      end
    end

    context 'with invalid params' do
      it 'assigns a newly created but unsaved page as @page' do
        post :create, { page: invalid_attributes }

        expect(assigns(:page)).to be_a_new(Page)
      end

      it "re-renders the 'new' template" do
        post :create, { page: invalid_attributes }

        expect(response).to render_template('new')
      end
    end
  end

  describe 'PUT #update' do
    context 'with valid params' do
      let(:author_new) { create(:author) }
      let(:new_attributes) { attributes_for(:page, author_id: author_new.id) }

      it 'updates the requested page' do
        page = create :page, valid_attributes

        put :update, { id: page.to_param, page: new_attributes }
        page.reload

        expect(page.body).to eq(new_attributes[:body])
        expect(page.author_id).to eq(new_attributes[:author_id])
        expect(page.title).to eq(new_attributes[:title])
        expect(page.slug).to eq(new_attributes[:slug])
      end

      it 'assigns the requested page as @page' do
        page = create :page, valid_attributes

        put :update, { id: page.to_param, page: valid_attributes }

        expect(assigns(:page)).to eq(page)
      end

      it 'redirects to the page' do
        page = create :page, valid_attributes

        put :update, { id: page.to_param, page: valid_attributes }

        expect(response).to redirect_to(page)
      end
    end

    context 'with invalid params' do
      it 'assigns the page as @page' do
        page = create :page, valid_attributes

        put :update, { id: page.to_param, page: invalid_attributes }

        expect(assigns(:page)).to eq(page)
      end

      it "re-renders the 'edit' template" do
        page = create :page, valid_attributes

        put :update, { id: page.to_param, page: invalid_attributes }

        expect(response).to render_template('edit')
      end
    end
  end

  describe 'DELETE #destroy' do
    it 'destroys the requested page' do
      page = create :page, valid_attributes

      expect {
        delete :destroy, { id: page.to_param }
      }.to change(Page, :count).by(-1)
    end

    it 'redirects to the pages list' do
      page = create :page, valid_attributes

      delete :destroy, { id: page.to_param }

      expect(response).to redirect_to(pages_url)
    end
  end

end
