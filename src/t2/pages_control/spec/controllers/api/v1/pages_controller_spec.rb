require 'rails_helper'

module Api
  module V1
    RSpec.describe PagesController, type: :controller do
      let(:auth_key) { FactoryGirl.create(:api_key).access_token }

      let(:author) { create(:author) }
      let(:valid_attributes) { attributes_for(:page, author_id: author.id) }
      let(:invalid_attributes) { attributes_for(:invalid_page) }

      before(:each) { request.headers['X-Api-Access-Token'] = auth_key }

      describe 'GET #index' do
        it 'assigns all pages as @pages' do
          page = create :page, valid_attributes

          get :index, format: :json

          expect(response.body).to eq([page].to_json)
        end
      end

      describe 'GET #show' do
        it 'assigns the requested page as @page' do
          page = create :page, valid_attributes

          get :show, { id: page.to_param, format: :json }

          expect(response.body).to eq(page.to_json)
        end
      end

      describe 'POST #create' do
        context 'with valid params' do
          it 'creates a new Page' do
            expect {
              post :create, { page: valid_attributes, format: :json }
            }.to change(Page, :count).by(1)
          end

          it 'redirects to the created page' do
            post :create, { page: valid_attributes, format: :json }

            expect(response).to be_created
          end
        end

        context 'with invalid params' do
          it "re-renders the 'new' template" do
            post :create, { page: invalid_attributes, format: :json }

            expect(response).to be_unprocessable
          end
        end
      end

      describe 'PUT #update' do
        context 'with valid params' do
          let(:author_new) { create(:author) }
          let(:new_attributes) { attributes_for(:page, author_id: author_new.id) }

          it 'updates the requested page' do
            page = create :page, valid_attributes

            put :update, { id: page.to_param, page: new_attributes, format: :json }
            page.reload

            expect(page.body).to eq(new_attributes[:body])
            expect(page.author_id).to eq(new_attributes[:author_id])
            expect(page.title).to eq(new_attributes[:title])
            expect(page.slug).to eq(new_attributes[:slug])
          end
        end

        context 'with invalid params' do
          it "re-renders the 'edit' template" do
            page = create :page, valid_attributes

            put :update, { id: page.to_param, page: invalid_attributes, format: :json }

            expect(response).to be_unprocessable
          end
        end
      end

      describe 'DELETE #destroy' do
        it 'destroys the requested page' do
          page = create :page, valid_attributes

          expect {
            delete :destroy, { id: page.to_param }
          }.to change(Page, :count).by(-1)
        end

        it 'redirects to the pages list' do
          page = create :page, valid_attributes

          delete :destroy, { id: page.to_param }

          expect(response).to redirect_to(pages_url)
        end
      end
    end
  end
end
