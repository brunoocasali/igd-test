require 'rails_helper'

module Api
  module V1
    RSpec.describe AuthorsController, type: :controller do
      let(:auth_key) { FactoryGirl.create(:api_key).access_token }
      let(:valid_attributes) { FactoryGirl.attributes_for(:author) }
      let(:invalid_attributes) { FactoryGirl.attributes_for(:invalid_author) }

      before(:each) { request.headers['X-Api-Access-Token'] = auth_key }

      describe 'GET #index' do
        it 'assigns all authors as @authors' do
          author = create :author

          get :index, format: :json

          expect(response.body).to eq([author].to_json)
        end
      end

      describe 'POST #create' do
        context 'with valid params' do
          it 'creates a new Author' do
            expect {
              post :create, { author: valid_attributes, format: :json }
            }.to change(Author, :count).by(1)
          end

          it 'assigns a newly created author as @author' do
            post :create, { author: valid_attributes, format: :json }

            expect(response).to be_created
          end
        end

        context 'with invalid params' do
          it 'return author for unprocessed entity' do
            post :create, author: invalid_attributes, format: :json

            expect(response).to be_unprocessable
          end
        end
      end

      describe 'PUT #update' do
        context 'with valid params' do
          let(:new_attributes) { FactoryGirl.attributes_for(:author) }

          it 'updates the requested author' do
            author = create :author, valid_attributes

            put :update, { id: author.to_param, author: new_attributes, format: :json }
            author.reload

            expect(author.email).to eq(new_attributes[:email])
            expect(author.name).to eq(new_attributes[:name])
          end
        end

        context 'with invalid params' do
          it "re-renders the 'edit' template" do
            author = create :author, valid_attributes

            put :update, { id: author.to_param, author: invalid_attributes, format: :json }

            expect(response).to be_unprocessable
          end
        end
      end

      describe 'DELETE #destroy' do
        it 'destroys the requested author' do
          author = create :author, valid_attributes
          expect {
            delete :destroy, { id: author.to_param }
          }.to change(Author, :count).by(-1)
        end

        it 'redirects to the authors list' do
          author = create :author, valid_attributes

          delete :destroy, { id: author.to_param }

          expect(response).to redirect_to(authors_url)
        end
      end
    end
  end
end
