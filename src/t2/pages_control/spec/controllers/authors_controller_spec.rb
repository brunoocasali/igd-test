require 'rails_helper'

RSpec.describe AuthorsController, type: :controller do

  let(:valid_attributes) { FactoryGirl.attributes_for(:author) }
  let(:invalid_attributes) { FactoryGirl.attributes_for(:invalid_author) }

  describe 'GET #index' do
    it 'assigns all authors as @authors' do
      author = create :author

      get :index

      expect(assigns(:authors)).to match_array([author])
    end
  end

  describe 'GET #new' do
    it 'assigns a new author as @author' do
      get :new

      expect(assigns(:author)).to be_a_new(Author)
    end
  end

  describe 'GET #edit' do
    it 'assigns the requested author as @author' do
      author = create :author, valid_attributes

      get :edit, { id: author.to_param }

      expect(assigns(:author)).to eq(author)
    end
  end

  describe 'POST #create' do
    context 'with valid params' do
      it 'creates a new Author' do
        expect {
          post :create, { author: valid_attributes }
        }.to change(Author, :count).by(1)
      end

      it 'assigns a newly created author as @author' do
        post :create, { author: valid_attributes }

        expect(assigns(:author)).to be_a(Author)
        expect(assigns(:author)).to be_persisted
      end

      it 'redirects to the created author' do
        post :create, { author: valid_attributes }

        expect(response).to redirect_to(authors_url)
      end
    end

    context 'with invalid params' do
      it 'assigns a newly created but unsaved author as @author' do
        post :create, { author: invalid_attributes}

        expect(assigns(:author)).to be_a_new(Author)
      end

      it "re-renders the 'new' template" do
        post :create, { author: invalid_attributes}

        expect(response).to render_template('new')
      end
    end
  end

  describe 'PUT #update' do
    context 'with valid params' do
      let(:new_attributes) { FactoryGirl.attributes_for(:author) }

      it 'updates the requested author' do
        author = create :author, valid_attributes

        put :update, { id: author.to_param, author: new_attributes }
        author.reload

        expect(author.email).to eq(new_attributes[:email])
        expect(author.name).to eq(new_attributes[:name])
      end

      it 'assigns the requested author as @author' do
        author = create :author, valid_attributes

        put :update, { id: author.to_param, author: valid_attributes }

        expect(assigns(:author)).to eq(author)
      end

      it 'redirects to the author' do
        author = create :author, valid_attributes

        put :update, { id: author.to_param, author: valid_attributes }

        expect(response).to redirect_to(authors_url)
      end
    end

    context 'with invalid params' do
      it 'assigns the author as @author' do
        author = create :author, valid_attributes

        put :update, { id: author.to_param, author: invalid_attributes }

        expect(assigns(:author)).to eq(author)
      end

      it "re-renders the 'edit' template" do
        author = create :author, valid_attributes

        put :update, { id: author.to_param, author: invalid_attributes }

        expect(response).to render_template('edit')
      end
    end
  end

  describe 'DELETE #destroy' do
    it 'destroys the requested author' do
      author = create :author, valid_attributes
      expect {
        delete :destroy, { id: author.to_param }
      }.to change(Author, :count).by(-1)
    end

    it 'redirects to the authors list' do
      author = create :author, valid_attributes

      delete :destroy, { id: author.to_param }

      expect(response).to redirect_to(authors_url)
    end
  end
end
