require 'rails_helper'

RSpec.describe Author, type: :model do
  context 'database associations' do
    it { is_expected.to have_many(:pages) }
  end

  context 'model validations' do
    it { is_expected.to validate_presence_of(:email) }
    it { is_expected.to validate_presence_of(:name) }
  end

  context 'model attributes' do
    it { is_expected.to have_db_column(:name).of_type(:string) }
    it { is_expected.to have_db_column(:email).of_type(:string) }
  end

  context 'database indexes' do
    it { is_expected.to have_db_index(:email).unique(true) }
  end
end
