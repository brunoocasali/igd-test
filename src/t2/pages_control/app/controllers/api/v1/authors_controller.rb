module Api
  module V1
    class AuthorsController < ApplicationController
      def index
        respond_with(Author.page)
      end

      def show
        respond_with(Author.find(params[:id]))
      end

      def create
        respond_with(Author.create(author_params), location: authors_path)
      end

      def update
        respond_with(Author.update(params[:id], author_params), location: authors_path)
      end

      def destroy
        respond_with(Author.destroy(params[:id]), location: authors_path)
      end

      private

      def author_params
        params.require(:author).permit(:name, :email)
      end
    end
  end
end
