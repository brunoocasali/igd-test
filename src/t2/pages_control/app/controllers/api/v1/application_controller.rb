module Api
  module V1
    class ApplicationController < ::ApplicationController
      respond_to :json
      protect_from_forgery with: :exception

      before_filter :cors_preflight_check, :restrict_access
      after_filter :cors_set_access_control_headers

      def cors_set_access_control_headers
        headers['Access-Control-Allow-Origin'] = '*'
        headers['Access-Control-Allow-Methods'] = 'POST, GET, PUT, DELETE, OPTIONS'
        headers['Access-Control-Allow-Headers'] = 'Origin, Content-Type, Accept, Authorization, Token, X-Api-Access-Token'
        headers['Access-Control-Max-Age'] = "1728000"
      end

      def cors_preflight_check
        if request.method == 'OPTIONS'
          headers['Access-Control-Allow-Origin'] = '*'
          headers['Access-Control-Allow-Methods'] = 'POST, GET, PUT, DELETE, OPTIONS'
          headers['Access-Control-Allow-Headers'] = 'X-Requested-With, X-Prototype-Version, Token, X-Api-Access-Token'
          headers['Access-Control-Max-Age'] = '1728000'

          render :text => '', :content_type => 'text/plain'
        end
      end

      private

      def restrict_access
        token = request.headers['X-Api-Access-Token']

        unless ApiKey.exists?(access_token: token)
          head status: :unauthorized
          false
        end
      end
    end
  end
end
