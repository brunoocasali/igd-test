module Api
  module V1
    class PagesController < ApplicationController
      before_action :set_page, except: [:index, :create]

      def index
        respond_with(Page.page)
      end

      def show
        respond_with(@page)
      end

      def create
        @page = Page.new(page_params)
        @page.save

        respond_with(@page, location: pages_path)
      end

      def update
        @page.update(page_params)

        respond_with(@page, location: pages_path)
      end

      def destroy
        @page.destroy

        respond_with(@page, location: pages_path)
      end

      private

      def set_page
        @page = Page.friendly.find(params[:id])
      end

      def page_params
        params.require(:page).permit(:title, :description, :body, :author_id)
      end
    end
  end
end
