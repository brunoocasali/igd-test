class Page < ActiveRecord::Base
  extend FriendlyId

  belongs_to :author

  friendly_id :title, use: :slugged

  validates :author, presence: true, associated: true

  validates :description, presence: true
  validates :slug, presence: true, uniqueness: true
  validates :title, presence: true
  validates :body, presence: true
end
