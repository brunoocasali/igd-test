class CreateAuthors < ActiveRecord::Migration
  def change
    create_table :authors do |t|
      t.string :name
      t.string :email

      t.timestamps null: false
    end

    add_index :authors, :email, unique: true
  end
end
