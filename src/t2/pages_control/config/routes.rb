require 'api_constraints'

Rails.application.routes.draw do

  namespace :api, defaults: { format: :json } do
    match '*any' => 'application#options', via: :options

    scope module: :v1, constraints: ApiConstraints.new(version: 1, default: true), except: [:new, :edit] do
      resources :pages
      resources :authors
    end
  end

  resources :pages
  resources :authors, except: :show

  root to: 'pages#index'
end
