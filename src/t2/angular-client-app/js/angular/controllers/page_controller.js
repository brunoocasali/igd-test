var myApp = angular.module('AngularClientPages', ['ngRoute', 'ngResource']);

//Factory
myApp.factory('pages', ['$resource', function($resource){
    return $resource('https://igd.herokuapp.com/api/pages.json', {}, {
        query: { method: 'GET', isArray: true, headers: { 'Access-Control-Allow-Origin': '*', 'X-Api-Access-Token': 'e1ae8e1eb755db9d8bb915d08f178fef' }  },
        create: { method: 'POST', headers: { 'Access-Control-Allow-Origin': '*', 'X-Api-Access-Token': 'e1ae8e1eb755db9d8bb915d08f178fef' }  }
    })
}]);


myApp.factory('Page', ['$resource', function($resource){
    return $resource('https://igd.herokuapp.com/api/pages/:id.json', {}, {
        show: { method: 'GET', headers: { 'Access-Control-Allow-Origin': '*', 'X-Api-Access-Token': 'e1ae8e1eb755db9d8bb915d08f178fef' } },
        update: { method: 'PUT', params: { id: '@id' }, headers: { 'Access-Control-Allow-Origin': '*', 'X-Api-Access-Token': 'e1ae8e1eb755db9d8bb915d08f178fef' } },
        delete: { method: 'DELETE', params: { id: '@id', headers: { 'Access-Control-Allow-Origin': '*', 'X-Api-Access-Token': 'e1ae8e1eb755db9d8bb915d08f178fef' } } }
    });
}]);

//Controller
myApp.controller("PageListCtr", ['$scope', '$http', '$resource', 'pages', 'Page', '$location', function($scope, $http, $resource, pages, Page, $location) {
    $scope.pages = pages.query();

    $scope.deletePage = function (userId) {
        if (confirm("Are you sure you want to delete this page?")){
            Page.delete({ id: userId }, function(){
                $scope.pages = pages.query();
                $location.path('/');
            });
        }
    };
}]);

myApp.controller("PageUpdateCtr", ['$scope', '$resource', 'Page', '$location', '$routeParams', function($scope, $resource, Page, $location, $routeParams) {
    $scope.page = Page.get({id: $routeParams.id});
    $scope.update = function(){
        if ($scope.pageForm.$valid){
            Page.update({id: $scope.page.id},{page: $scope.page},function(){
                $location.path('/');
            }, function(error) {
                console.log(error)
            });
        }
    };
}]);

myApp.controller("PageAddCtr", ['$scope', '$http', '$resource', 'pages', '$location', function($scope, $http, $resource, pages, $location) {

    //$.ajax({
    //    url: 'https://igd.herokuapp.com/api/authors.json',
    //    type: 'GET',
    //    dataType: 'json',
    //    success: function(data) { $scope.authors = data; },
    //    beforeSend: function(xhr) {
    //        xhr.setRequestHeader('X-Api-Access-Token', 'e1ae8e1eb755db9d8bb915d08f178fef');
    //    }
    //});

    $scope.page = {};

    $scope.save = function () {
        if ($scope.pageForm.$valid){
            pages.create({page: $scope.page}, function(){
                $location.path('/');
            }, function(error){
                console.log(error)
            });
        }
    };
}]);

//Routes
myApp.config([
    '$routeProvider', '$locationProvider', function($routeProvider, $locationProvider) {
        $routeProvider.when('/pages',{
            templateUrl: './templates/pages/index.html',
            controller: 'PageListCtr'
        });
        $routeProvider.when('/pages/new', {
            templateUrl: './templates/pages/new.html',
            controller: 'PageAddCtr'
        });
        $routeProvider.when('/pages/:id/edit', {
            templateUrl: './templates/pages/edit.html',
            controller: "PageUpdateCtr"
        });
        $routeProvider.otherwise({
            redirectTo: '/pages'
        });
    }
]);
